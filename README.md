
# Leafonyベースの移動体制御へのLTE-Mの組込み: 接続事例報告

現在Leafonyベースの移動体制御を検討している。移動体としては ラジコン・カーとUAV(ドローン)が目標。これらにLTE-M受信機を組込む方法を検討した。

目的としては、LTE-Mモジュールとしては既存の製品を流用する事です。

- 実施例１: さくらのモノプラットフォームβ への接続
- 実施例２: SORACOM Air への接続

## さくらのモノプラットフォームβ への接続

- 「sakura.io」から LTEモジュールと　評価ボードが提供されています
    ![sakura.io の開発ツール](images/Leafony-Sakura-LTE_1.png)

- LeafonyにもArduinoシールドがあるので、上記と重ね合わせて使えます。
    ![Leafony・さくらモノプラットフォームβ連携](images/Leafony-Sakura-LTE_2.png)
  - マイコンリーフはAVR
  
## XBeeソケット互換の LTE-M モデム

- ソラコムからは LTE-M Shield for Arduino が提供されています。
- Arduinoシールドは使わず、NB-IoT-Bee-BG96 モデムだけを利用します。

## Leafony XBeeソケット 連携ボード

LeafonyバスとXBeeソケットを連携する基板を自作しました

   ![Leafony・XBeeソケット連携基板](images/Leafony-XBee_3-usage.png)

## SORACOM Air への接続

Arduinoシールドよりは、連携基板で小型に実装できました。

   ![Leafony・XBeeソケット連携 LTE-M](images/Leafony-BG96-XBee.png)

## Leafony XBeeソケット 連携ボード: 回路図 (3/3)

## 補足

[Leafonyの概要はこちら](https://docs.leafony.com/docs/overview/)
